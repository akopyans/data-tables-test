package com.sakopyan.test.page;

import java.util.ArrayList;
import java.util.List;

public class TableCol {
    private String title;
    private List<String> elements = new ArrayList<String>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getElements() {
        return elements;
    }

    public void addTextElement(String s) {
        elements.add(s);
    }
}
