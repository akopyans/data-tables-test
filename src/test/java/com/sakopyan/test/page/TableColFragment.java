package com.sakopyan.test.page;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class TableColFragment extends TableCol {
    private WebElement title;
    private List<WebElement> elements = new ArrayList<WebElement>();

    public WebElement getTitleElement() {
        return title;
    }

    public void setTitleElement(WebElement title) {
        this.title = title;
    }

    public List<WebElement> getValueElements() {
        return elements;
    }

    public void setValueElements(List<WebElement> elements) {
        this.elements = elements;
    }
}
