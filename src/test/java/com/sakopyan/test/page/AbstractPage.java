package com.sakopyan.test.page;

import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {
    private WebDriver driver;
    private String path;

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
