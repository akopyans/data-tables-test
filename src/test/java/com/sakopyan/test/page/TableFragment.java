package com.sakopyan.test.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class TableFragment {
    private List<TableColFragment> lsTableCols = new ArrayList<TableColFragment>();

    public TableFragment(WebElement table) {
        WebElement thead = table.findElement(By.tagName("thead"));
        WebElement tr = thead.findElement(By.tagName("tr"));
        List<WebElement> tds = tr.findElements(By.tagName("th"));

        for (int i=0;i<tds.size();i++) {
            WebElement title = tds.get(i);
            TableColFragment tableColFragment = new TableColFragment();
            tableColFragment.setTitleElement(title);
            tableColFragment.setTitle(title.getText());

            WebElement tbody = table.findElement(By.tagName("tbody"));
            List<WebElement> trs = tbody.findElements(By.tagName("tr"));
            for (WebElement tr1 : trs) {
                List<WebElement> lsTd = tr1.findElements(By.tagName("td"));

                WebElement td = lsTd.get(i);
                String text = td.getText();
                if (title.equals("Due") && text.startWith("$")) {
                    text = text.substring(1);
                }
                tableColFragment.addTextElement(text);
                lsTableCols.add(tableColFragment);
            }
        }
    }

    public List<TableColFragment> getTableCols() {
        return lsTableCols;
    }

}
