package com.sakopyan.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.ArrayList;
import java.util.List;

public class WelcomePage /*extends AbstractPage*/ {
    private final static String SORTABLE_DATA_TABLES = "Sortable Data Tables";

    private WebDriver driver;

    @FindBy(xpath = "//div[@id='content']/ul/li/a")
    private List<WebElement> lsAnchors = new ArrayList<WebElement>();

    @FindBy(tagName = "h1")
    private WebElement title;

    public WelcomePage(WebDriver driver) {
        this.driver = driver;
        //setDriver(driver);
        //setPath("/");
    }

    private WebElement getSortirableDataTablesLink() {
        for (WebElement we : lsAnchors) {
            if (we.getText().equals(SORTABLE_DATA_TABLES)) {
                return we;
            }
        }
        return null;
    }

    public void clickSortableDataTable() {
        getSortirableDataTablesLink().click();
    }

    private WebElement getTitleElement() {
        return title;
    }

    public String getTitle() {
        return getTitleElement().getText();
    }
}
