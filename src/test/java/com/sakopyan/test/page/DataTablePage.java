package com.sakopyan.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: akopyans
 * Date: 28.11.15
 * Time: 0:07
 * To change this template use File | Settings | File Templates.
 */
public class DataTablePage {
    public WebDriver driver;

    @FindBy(tagName = "h3")
    private WebElement title;

    @FindBy(className = "tablesorter")
    private List<WebElement> lsTables = new ArrayList<WebElement>();

    public DataTablePage(WebDriver driver) {
        this.driver = driver;
        //setDriver(driver);
        //setPath("/tables");
    }

    public List<WebElement> getTables() {
        return lsTables;
    }

    public String getTitle() {
        return title.getText();
    }
}
