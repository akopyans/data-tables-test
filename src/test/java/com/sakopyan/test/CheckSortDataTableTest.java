package com.sakopyan.test;

import com.sakopyan.test.page.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-config.xml")
public class CheckSortDataTableTest {

    private WelcomePage welcomePage;
    private DataTablePage dataTablePage;
    private TableFragment tableFragment;
    @Autowired
    private WebDriver driver;
    @Autowired
    private URL baseURL;
    @Autowired
    private Float max;

    @Before
    public void setUp() {
        driver.get(baseURL.toString());
    }

    private void checkWelcomePage() {
        welcomePage = PageFactory.initElements(driver, WelcomePage.class);
        assertEquals("Welcome to the Internet", welcomePage.getTitle());
    }

    private void checkDataTablePage() {
        welcomePage.clickSortableDataTable();
        dataTablePage = PageFactory.initElements(driver, DataTablePage.class);
        assertEquals("Data Tables", dataTablePage.getTitle());

        for (WebElement table : dataTablePage.getTables()) {
            tableFragment = new TableFragment(table);
            for (TableColFragment tcf : tableFragment.getTableCols()) {
                assertArrayEquals(table.getAttribute("id") + "\t" + tcf.getTitle(), getSortedElements(tcf.getElements()).toArray(), tcf.getElements().toArray());
            }
        }

        for (TableColFragment tcf : tableFragment.getTableCols()) {
            if ("Due".equals(tcf.getTitle())) {
                List<String> lsValues = tcf.getElements();

                /*for (int i = 0; i < lsValues.size(); i++) {
                    String val = lsValues.get(i);
                    val = val.replace("$", "");
                    lsValues.set(i, val);
                }*/

                Float current = Float.valueOf(Collections.max(tcf.getElements()));
                assertTrue(max > current);
            }
        }
    }

    @Test
    public void a_test() throws MalformedURLException {
        checkWelcomePage();
        checkDataTablePage();
        /*welcomePage = PageFactory.initElements(driver, WelcomePage.class);
        assertEquals("Welcome to the Internet", welcomePage.getTitle());*/
        //assertEquals(getURL(welcomePage), driver.getCurrentUrl());
    }
/*
    @Test
    public void b_test() throws MalformedURLException {
        welcomePage.clickSortableDataTable();
        dataTablePage = PageFactory.initElements(driver, DataTablePage.class);
        assertEquals("Data Tables", dataTablePage.getTitle());
        //assertEquals(getURL(dataTablePage), driver.getCurrentUrl());

        dataTablePage = PageFactory.initElements(driver, DataTablePage.class);
        for (WebElement table : dataTablePage.getTables()) {
            tableFragment = new TableFragment(table);
            for (TableColFragment tcf : tableFragment.getTableCols()) {
                assertArrayEquals(table.getAttribute("id") + "\t" + tcf.getTitle(), getSortedElements(tcf.getElements()).toArray(), tcf.getElements().toArray());
                //isSort(tcf.getElements());
                //Assert.fail(table.getAttribute("id") + "\t" + tcf.getTitle());
            }
        }

        for (TableColFragment tcf : tableFragment.getTableCols()) {
            if ("Due".equals(tcf.getTitle())) {
                System.out.println(tcf.getElements());
            }
        }
    }

    @Test
    @Ignore
    public void c_test() {
        dataTablePage = PageFactory.initElements(driver, DataTablePage.class);
        for (WebElement table : dataTablePage.getTables()) {
            tableFragment = new TableFragment(table);
            for (TableColFragment tcf : tableFragment.getTableCols()) {

                //isSort(tcf.getElements());
                //Assert.fail(table.getAttribute("id") + "\t" + tcf.getTitle());
            }
        }
    }

    @Test
    @Ignore
    public void d_test() {
        for (TableColFragment tcf : tableFragment.getTableCols()) {
            if ("Due".equals(tcf.getTitle())) {
                System.out.println(tcf.getElements());
            }
        }
    }*/

    private List<String> getSortedElements(List<String> lsElements) {
        List<String> sortList = new ArrayList<String>();
        sortList.addAll(lsElements);
        Collections.sort(sortList);
        return lsElements;
    }

    private String getURL(AbstractPage page) throws MalformedURLException {
        return new URL(baseURL, page.getPath()).toString();
    }
}
